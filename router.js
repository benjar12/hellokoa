var _ = require('koa-route');
var user = require("./routes/user.js");
var session = require('koa-generic-session');
var bodyParser = require('koa-bodyparser');
var passport = require('koa-passport');
require('./passport.js');

module.exports = function(app){

    // trust proxy
    app.proxy = true

    // sessions
    /* TODO Move this to a json config file */
    app.keys = ['Super-Secret-Key']
    app.use(session())

    // body parser
    app.use(bodyParser())

    // authentication
    app.use(passport.initialize())
    app.use(passport.session())

    // Request Log
    app.use(function *(next){
        //log start and end
        var start = new Date;
        yield next;
        var ms = new Date - start;
        console.log('%s %s - %s', this.method, this.url, ms);
        this.set("Request-Time", ms+"ms");
    });

    //TODO : Move this stuff to the routes/auth.js file
    
    //Methods for Facebook login
    app.use(_.get('/auth/facebook',
        passport.authenticate('facebook')
    ));

    app.use(_.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
          successRedirect: '/app',
          failureRedirect: '/'
        })
    ));

    //TODO: Move the state for is authed out of the app
    app.use(function*(next) {
      if (this.isAuthenticated() || this.url == "/") {
        yield next
      } else {
        this.redirect('/')
      }
    })

    // Provide method for getting a single user
    app.use(_.get('/user/:id', user.get));




}
