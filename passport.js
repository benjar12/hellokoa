var passport = require('koa-passport')
var users = require('./models/user.js')
var tokens = require('./models/auth.js')

var user = { id: 1, username: 'test' }

passport.serializeUser(function(user, done) {
  done(null, user.id)
})

passport.deserializeUser(function(id, done) {
  done(null, user)
})

var LocalStrategy = require('passport-local').Strategy
passport.use(new LocalStrategy(function(username, password, done) {
  // retrieve user ...
  if (username === 'test' && password === 'test') {
    done(null, user)
  } else {
    done(null, false)
  }
}))

var FacebookStrategy = require('passport-facebook').Strategy
//TODO Move the config stuff to a config file
passport.use(new FacebookStrategy({
    clientID: '992265564140731',
    clientSecret: '5bff80261d340bbada06a45f02b656fa',
    callbackURL: 'http://localhost:' + (process.env.PORT || 3000) + '/auth/facebook/callback'
  },
  function(token, tokenSecret, profile, done) {

    //define a general error handler
    function errHandler(msg){
      console.error(msg);
      done(msg);
    }

    console.log(profile);

    //Check if user exists in mongo
    var prom = users.findOne({usertype:"facebook", socialid: profile.id}).exec();

    // add a callback that is only called on err (rejected) events
    prom.addErrback(function () { errHandler("Mongo Error") });

    //if user does not exist create
    prom = prom.then(function(user){
      if(!user){
        return users.create({usertype:"facebook", socialid: profile.id});
      }else{
        return user
      }
    });

    //create auth token
    prom = prom.then(function(user){
      var id = user._id;
      var t = {token: token, secret: tokenSecret}
      tokens.create({type:"facebook", userid: id, token: t});
      return user;
    });

    prom.then(function(user){
      console.log("made it here");
      done(null, user);
    });

  }
))
