var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var crypto = require('crypto');

var Schema = mongoose.Schema;

var tokensSchema = new mongoose.Schema({
  userid:     { type: Schema.Types.ObjectId },
  type:       { type: String },
  token:      { type: Object },
  dateCreated:{ type: Date, default: new Date() }
});

tokensSchema.index({ userid: 1, type: 1, dateCreated: -1 }, { unique: false });

module.exports = mongoose.model('Token', tokensSchema);
