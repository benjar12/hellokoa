var koa = require('koa');
var app = koa();
require("./router.js")(app);
//TODO Move this to a json config file
app.listen(3000);

var mongoose = require("mongoose")
mongoose.connect('mongodb://localhost:27017/HelloKoa', { server : { autoReconnect: true, socketOptions: { connectTimeoutMS: 10000 } } });
mongoose.connection.on('error', function(err) {
  console.error('MONGO ERROR: Something broke!');
  console.log(err);
});
